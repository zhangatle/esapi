<?php
/**
 * Created by PhpStorm.
 * User: zhang
 * Date: 2019/8/26
 * Email: zhangatle@gmail.com
 */

namespace App\Lib\Upload;


class Txt extends Base
{
    public $fileType = "video";
    public $maxSize = 122;
}